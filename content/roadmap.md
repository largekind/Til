---
title: "Roadmap"
date: 2024-05-24T22:26:45+09:00
---

# Roadmap

## 概要

今後追加したい項目などをメモとして残す用の場所

例えば問題演習などで分からなかった単語などをスマホ等からメモとして残したいときに記載する

## メモ欄

- ONU
- TOBEモデル
- SMNPトラップ
- CALS
- Web-EDI
- バーチャルカンパニー/モール
- エンプロイヤビリティ
- CPS
- ファイアウォールの方式(パケットフィルタリングやトランスポート、アプリケーションゲートウェイなど)
- PKI
- LRU FIFOなど忘れやすいメモリ取り扱い方式関係
- ディスクの平均読取時間の解き方