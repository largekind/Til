---
title: "EDI"
date: 2024-04-07T21:39:31+09:00
draft: True
categories: ["InfoEngineer"]
tags: ["InfoEngineer", "strategy"]
---
# EDI

## 概要

Electronic Data Interchange（電子データ交換）

企業間における契約書や受発注などの商取引文書を、通信回線を通じてやり取りする仕組みのこと

ペーパーレスや業務効率化などができる
