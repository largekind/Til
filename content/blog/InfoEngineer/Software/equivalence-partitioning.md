---
title: "Equivalence Partitioning"
date: 2024-03-11T21:17:52+09:00
draft: True
categories: ["InfoEngineer"]
tags: ["InfoEngineer", "Software"]
---
# Equivalence Partitioning

## 概要

同値分割（equivalence partitioning）

ソフトウェアテストで適切なテストケースを作成する手法の一つで、出力が同じになるような入力をそれぞれグループにまとめ、グループの中から代表を選んでテストを行なう方式