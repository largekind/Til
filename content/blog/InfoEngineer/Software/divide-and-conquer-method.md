---
title: "Divide and Conquer Method"
date: 2024-04-19T19:41:28+09:00
---

# Divide and Conquer Method

## 概要

分割統治法

クイックソートやマージソートで使われている、大きな問題を同じ構造をもつ小さな構造に分割し、最後に統合することで元の問題を解決しようという考え方
