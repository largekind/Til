---
title: "P2P"
date: 2024-04-09T23:19:53+09:00
draft: True
categories: ["InfoEngineer"]
tags: ["InfoEngineer", "Software"]
---
# P2P

## 概要

どの端末もサーバにもなればクライアントにもなるという特徴を持つシステムの一形態

データ管理を行うためにフラッディング(Flooding)と呼ばれる幅優先探索が使われている
